# Alpine Linux aports

A personal collection of APKBUILDs for Alpine Linux. **Use at your own risk**.

## Packages

- [soft-serve](https://github.com/charmbracelet/soft-serve)
- [gotosocial](https://github.com/superseriousbusiness/gotosocial)
- [routedns](https://github.com/folbricht/routedns)
- deleted
    - [nats-server](https://github.com/nats-io/nats-server)
    - [vaultwarden](https://github.com/dani-garcia/vaultwarden)
    - [ntfy](https://github.com/binwiederhier/ntfy)
    - [unbound](https://www.nlnetlabs.nl/projects/unbound/about/)
        - from [alpine/aports](https://gitlab.alpinelinux.org/alpine/aports/-/tree/master/main/unbound)
        - added nghttp2 to support DoH
